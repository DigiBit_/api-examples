// ========================================================================================
// ArkadePluginController.m
// ArkadePluginController
// 2018 Copyright © DigiBit LLC - All rights reserved
//

#import "iospluginArkade.h"
#import "UnityAppController.h"


@interface ArkadePluginAppController : UnityAppController

@property ( retain, nonatomic ) iospluginArkade* iosPlugin;

-(void) initNativePlugin;

@end

static ArkadePluginAppController* delegateObject;

@implementation ArkadePluginAppController

@synthesize iosPlugin;


//PLUGIN INITIALIZATION FROM UNITY
-(void) startUnity: (UIApplication*) application
{
     [super startUnity: application];  //call the super.
     [self initNativePlugin];
     delegateObject = self;
}

-(void) initNativePlugin
{
     iosPlugin = [[ iospluginArkade alloc ] init];
}
//PLUGIN INITIALIZATION FROM UNITY


//PLUGIN CALLS FROM PLUGINCONRTOLLER

-(void) setObjectName_:(NSString*) objectName
{
     [iosPlugin setObjectName:objectName ];
}

-(bool) sendCommand_:(NSString*) command
{
     return [iosPlugin sendCommand:command ];
}

-(NSString*) getPidData_
{
     return [iosPlugin getPidData];
}

-(void) enableLogs_:(bool) enable
{
     [iosPlugin enableLogs:enable ];
}

-(void) onApplicationPaused_:(bool) paused
{
     [iosPlugin onApplicationPaused:paused ];
}

//PLUGIN CALLS FROM PLUGINCONRTOLLER




//PLUGIN CALLS FROM UNITY
extern "C"
{

     void SetObjectName( const char* _objectName )
     {
          NSString* objectName = [[NSString alloc] initWithUTF8String:_objectName];
          [delegateObject setObjectName_:objectName];
     }

     bool SendCommand( const char* _command )
     {
          NSString* command = [[NSString alloc] initWithUTF8String:_command];
          return [delegateObject sendCommand_:command];
     }

     char* GetPidData()
     {
          NSString* data = [delegateObject getPidData_];
          return (char*) [data UTF8String];
     }

     void EnableLogs( bool enable )
     {
          [delegateObject enableLogs_:enable];
     }

     void OnApplicationPaused( bool paused )
     {
          [delegateObject onApplicationPaused_:paused];
     }
}
//PLUGIN CALLS FROM UNITY

@end

//setting this as app controller.
IMPL_APP_CONTROLLER_SUBCLASS( ArkadePluginAppController)
