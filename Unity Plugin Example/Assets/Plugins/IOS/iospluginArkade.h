// ========================================================================================
// iospluginArkade.h
// iospluginArkade
// 2018 Copyright © DigiBit LLC - All rights reserved
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface iospluginArkade : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>

+( iospluginArkade * ) plugin;
-(id) init;
-(void) setObjectName: (NSString*) objectName;
-(bool) sendCommand: (NSString*) command;
-(NSString *) getPidData;
-(void) enableLogs: (bool) enable;
-(void) onApplicationPaused: (bool) paused;
@end
