﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
using UnityEngine;
using System.Collections;

public class RotateThis : MonoBehaviour
{
    public Vector3 rotateVector = new Vector3(0, 0, -1);
    public float speed = 5.0f;

     void Update ()
    {
          transform.Rotate(rotateVector * speed);
     }
}
