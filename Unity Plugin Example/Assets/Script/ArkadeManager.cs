// ========================================================================================
// 2018 Copyright � DigiBit LLC - All rights reserved
//
using UnityEngine;
using System.Collections;
using ArkadeSdk;

public class ArkadeManager : MonoBehaviour {

     //Tag for Debugging 
     const string TAG = "Arkade-Demo-Unity "; 

     void Start ()
     {
          Screen.sleepTimeout = SleepTimeout.NeverSleep;

          ArkadeBlaster.OnDeviceConnected += ArkadeBlaster_OnDeviceConnected;
          ArkadeBlaster.OnDeviceDisconnected += ArkadeBlaster_OnDeviceDisconnected;

          //Initializes Arkade
          ArkadeBlaster.Init(true, true);
     }

     void ArkadeBlaster_OnDeviceConnected()
     {
          Debug.Log(TAG + "onDeviceDisconnected");
     }

     void ArkadeBlaster_OnDeviceDisconnected()
     {
          Debug.Log(TAG + "OnDeviceConnected::");
     }
}
