﻿// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using ArkadeSdk;

public class GameManager : MonoBehaviour
{

     public Text connectionStatus;
     public Text[] eulerAngle;
     public Text[] angularVelocity;
     public Text[] linearVelocity;
     public Text[] buttons;
     public Text[] motionStates;
     public Text[] eulerMultiplierTextInput;

     public GameObject mainCanvas;
     public GameObject ledControlCanvas;
     public Button highResButton;
     public Slider healthSlider;
     public Text healthValue;
     public Text filterValue;
     public Text highResText;
     public bool highRes = false;


     Color keyDownColor = Color.black;
     Color keyUpColor = Color.white;

     void Update()
     {

          connectionStatus.text = "Connection Status: " + ArkadeBlaster.connected;

          eulerAngle[0].text = "X = " + ArkadeBlaster.Inputs.eulerAngle.x.ToString("0.00").PadLeft(6, ' ') + "°";
          eulerAngle[1].text = "Y = " + ArkadeBlaster.Inputs.eulerAngle.y.ToString("0.00").PadLeft(6, ' ') + "°";
          eulerAngle[2].text = "Z = " + ArkadeBlaster.Inputs.eulerAngle.z.ToString("0.00").PadLeft(6, ' ') + "°";

          angularVelocity[0].text = "X = " + ArkadeBlaster.Inputs.angularVelocity.x.ToString("0.00").PadLeft(7, ' ') + "°/sec";
          angularVelocity[1].text = "Y = " + ArkadeBlaster.Inputs.angularVelocity.y.ToString("0.00").PadLeft(7, ' ') + "°/sec";
          angularVelocity[2].text = "Z = " + ArkadeBlaster.Inputs.angularVelocity.z.ToString("0.00").PadLeft(7, ' ') + "°/sec";

          linearVelocity[0].text = "X = " + ArkadeBlaster.Inputs.linearVelocity.x.ToString("0.00").PadLeft(7, ' ') + "cm/s";
          linearVelocity[1].text = "Y = " + ArkadeBlaster.Inputs.linearVelocity.y.ToString("0.00").PadLeft(7, ' ') + "cm/s";
          linearVelocity[2].text = "Z = " + ArkadeBlaster.Inputs.linearVelocity.z.ToString("0.00").PadLeft(7, ' ') + "cm/s";

          buttons[0].text = ArkadeBlaster.Inputs.buttonA.key.ToString();
          buttons[1].text = ArkadeBlaster.Inputs.buttonB.key.ToString();
          buttons[2].text = ArkadeBlaster.Inputs.buttonC.key.ToString();
          buttons[3].text = ArkadeBlaster.Inputs.buttonD.key.ToString();
          buttons[4].text = ArkadeBlaster.Inputs.buttonE.key.ToString();
          buttons[5].text = ArkadeBlaster.Inputs.buttonF.key.ToString();
          buttons[6].text = ArkadeBlaster.Inputs.buttonX.key.ToString();
          buttons[7].text = ArkadeBlaster.Inputs.buttonY.key.ToString();
          buttons[8].text = ArkadeBlaster.Inputs.thumb1.key.ToString();
          buttons[9].text = ArkadeBlaster.Inputs.thumb2.key.ToString();
          buttons[10].text = ArkadeBlaster.Inputs.fire.key.ToString();
          buttons[11].text = ArkadeBlaster.Inputs.backward.key.ToString();
          buttons[12].text = ArkadeBlaster.Inputs.forward1.key.ToString();
          buttons[13].text = ArkadeBlaster.Inputs.forward2.key.ToString();

          if (ArkadeBlaster.Inputs.buttonA.keyDown)
               buttons[0].color = keyDownColor;
          if (ArkadeBlaster.Inputs.buttonA.keyUp)
               buttons[0].color = keyUpColor;

          motionStates[0].text = ArkadeBlaster.Inputs.moveUp.ToString();
          motionStates[1].text = ArkadeBlaster.Inputs.moveDown.ToString();
          motionStates[2].text = ArkadeBlaster.Inputs.moveLeft.ToString();
          motionStates[3].text = ArkadeBlaster.Inputs.moveRight.ToString();
          motionStates[4].text = ArkadeBlaster.Inputs.moveForward.ToString();
          motionStates[5].text = ArkadeBlaster.Inputs.moveBackward.ToString();
          motionStates[6].text = ArkadeBlaster.Inputs.tap.ToString();
          motionStates[7].text = ArkadeBlaster.Inputs.turnRight.ToString();
          motionStates[8].text = ArkadeBlaster.Inputs.turnLeft.ToString();
          motionStates[9].text = ArkadeBlaster.Inputs.leanLeft.ToString();
          motionStates[10].text = ArkadeBlaster.Inputs.leanRight.ToString();
          motionStates[11].text = ArkadeBlaster.Inputs.reload.ToString();
          motionStates[12].text = ArkadeBlaster.Inputs.pickUpObject.ToString();
          motionStates[13].text = ArkadeBlaster.Inputs.celebrate.ToString();
          motionStates[14].text = ArkadeBlaster.Inputs.swapWeapon.ToString();
          motionStates[15].text = ArkadeBlaster.Inputs.knifeAttack.ToString();
          motionStates[16].text = ArkadeBlaster.Inputs.shield.ToString();
     }

     public void ShowMainCanvas()
     {
          mainCanvas.SetActive(true);
          ledControlCanvas.SetActive(false);
     }

     public void ShowLedControlCanvas()
     {
          mainCanvas.SetActive(false);
          ledControlCanvas.SetActive(true);
     }

     public void OnSliderValueChange()
     {
          healthValue.text = healthSlider.value.ToString();
     }

     public void OnGameStateUpdate(int gameState)
     {
          switch (gameState)
          {
               case 0:
                    ArkadeBlaster.StartEndGame(GameStates.VICTORY);
                    break;
               case 1:
                    ArkadeBlaster.StartEndGame(GameStates.DEFEAT);
                    break;
               case 2:
                    ArkadeBlaster.StartEndGame(GameStates.START);
                    break;

          }
     }

     public void OnPlayerHealthUpdate(int damageState)
     {
          switch (damageState)
          {
               case 0:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.NONE);
                    break;
               case 1:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.HEALING);
                    break;
               case 2:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.POISON_ATTACK);
                    break;
               case 3:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.TAKING_DAMAGE);
                    break;
               case 4:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.EXPLOSIVE);
                    break;
               case 5:
                    ArkadeBlaster.SetHealth((byte)healthSlider.value, DamageStates.FLASH_BANGS);
                    break;
          }
     }

     public void OnEulerMultiplierUpdateX(string newValue)
     {
          ArkadeBlaster.eulerMultiplier.x = float.Parse(eulerMultiplierTextInput[0].text);
          eulerMultiplierTextInput[0].text = ArkadeBlaster.eulerMultiplier.x.ToString("0.00");
     }

     public void OnEulerMultiplierUpdateY(string newValue)
     {
          ArkadeBlaster.eulerMultiplier.y = float.Parse(eulerMultiplierTextInput[1].text);
          eulerMultiplierTextInput[1].text = ArkadeBlaster.eulerMultiplier.y.ToString("0.00");
     }

     public void OnEulerMultiplierUpdateZ(string newValue)
     {
          ArkadeBlaster.eulerMultiplier.z = float.Parse(eulerMultiplierTextInput[2].text);
          eulerMultiplierTextInput[2].text = ArkadeBlaster.eulerMultiplier.z.ToString("0.00");
     }

     public void ApplyFilter()
     {
          ArkadeBlaster.SetFilterStrength(byte.Parse(filterValue.text));
     }

     public void OnSetHighResMode()
     {
          highRes = !highRes;
          ArkadeBlaster.SetHighResMode(highRes);
          ColorBlock cb = highResButton.colors;

          if (highRes)
          {
               highResText.text = "HI Res ON";
               cb.normalColor = Color.gray;
          }
          else
          {
               cb.normalColor = Color.white;
               highResText.text = "HI Res OFF";
          }

          highResButton.colors = cb;
     }

}
