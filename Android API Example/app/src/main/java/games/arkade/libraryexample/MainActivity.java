// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
package games.arkade.libraryexample;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import games.arkade.arkadeblaster.ArkadeBlaster;
import games.arkade.arkadeblaster.ArkadeBlasterListener;

public class MainActivity extends AppCompatActivity
{
     final Handler uiUpdateHandler = new Handler();

     //Views
     private TextView deviceStatusView;
     private TextView eulerXView;
     private TextView eulerYView;
     private TextView eulerZView;
     private TextView linearXView;
     private TextView linearYView;
     private TextView linearZView;
     private TextView angularXView;
     private TextView angularYView;
     private TextView angularZView;
     private TextView buttonXView;
     private TextView buttonYView;
     private TextView buttonAView;
     private TextView buttonBView;
     private TextView buttonCView;
     private TextView buttonDView;
     private TextView buttonEView;
     private TextView buttonFView;
     private TextView thumb1View;
     private TextView thumb2View;
     private TextView fireView;
     private TextView backwardView;
     private TextView forward1View;
     private TextView forward2View;
     private TextView moveUpView;
     private TextView moveDownView;
     private TextView moveLeftView;
     private TextView moveRightView;
     private TextView moveForwardView;
     private TextView moveBackwardView;
     private TextView tap;
     private TextView turnLeftView;
     private TextView turnRightView;
     private TextView leanLeftView;
     private TextView leanRightView;
     private TextView reloadView;
     private TextView pickUpObjectView;
     private TextView celebrateView;
     private TextView swapWeaponView;
     private TextView shieldView;
     private TextView knifeAttackView;

     private EditText multiplierXText;
     private EditText multiplierYText;
     private EditText multiplierZText;
     SeekBar strengthBar;
     TextView strengthText;


     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);
//          Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//          setSupportActionBar(toolbar);

          ArkadeBlaster.setArkadeBlasterListener(new ArkadeBlasterListener()
          {
               @Override
               public void onConnected()
               {
                    Log.d("arkadeblaster", "On connected Called");

                    //ArkadeBlaster.setHealth(100, ArkadeBlaster.DamageStates.NONE);
               }

               @Override
               public void onDisconnected()
               {
                    Log.d("arkadeblaster", "On Disconnected Called");
               }

               @Override
               public void onResetMotionResponse(boolean reset)
               {
                    Log.d("arkadeblaster", "On Reset Motion State Response Called " + reset);
               }
          });
          getUI();
          ArkadeBlaster.init(this, true);
          uiUpdate.run();
     }

     public void getUI()
     {
          deviceStatusView = (TextView) findViewById(R.id.device_status);
          eulerXView = (TextView) findViewById(R.id.euler_x);
          eulerYView = (TextView) findViewById(R.id.euler_y);
          eulerZView = (TextView) findViewById(R.id.euler_z);
          linearXView = (TextView) findViewById(R.id.linear_x);
          linearYView = (TextView) findViewById(R.id.linear_y);
          linearZView = (TextView) findViewById(R.id.linear_z);
          angularXView = (TextView) findViewById(R.id.angular_x);
          angularYView = (TextView) findViewById(R.id.angular_y);
          angularZView = (TextView) findViewById(R.id.angular_z);
          buttonXView = (TextView) findViewById(R.id.buttonX);
          buttonYView = (TextView) findViewById(R.id.buttonY);
          buttonAView = (TextView) findViewById(R.id.buttonA);
          buttonBView = (TextView) findViewById(R.id.buttonB);
          buttonCView = (TextView) findViewById(R.id.buttonC);
          buttonDView = (TextView) findViewById(R.id.buttonD);
          buttonEView = (TextView) findViewById(R.id.buttonE);
          buttonFView = (TextView) findViewById(R.id.buttonF);
          thumb1View = (TextView) findViewById(R.id.thumb1);
          thumb2View = (TextView) findViewById(R.id.thumb2);
          fireView = (TextView) findViewById(R.id.fire);
          backwardView = (TextView) findViewById(R.id.backward);
          forward1View = (TextView) findViewById(R.id.forward1);
          forward2View = (TextView) findViewById(R.id.forward2);


          moveUpView = (TextView) findViewById(R.id.move_up);
          moveDownView = (TextView) findViewById(R.id.move_down);
          moveLeftView = (TextView) findViewById(R.id.move_left);
          moveRightView = (TextView) findViewById(R.id.move_right);
          moveForwardView = (TextView) findViewById(R.id.move_forward);
          moveBackwardView = (TextView) findViewById(R.id.move_backward);
          tap = (TextView) findViewById(R.id.tap);
          turnLeftView = (TextView) findViewById(R.id.turn_left);
          turnRightView = (TextView) findViewById(R.id.turn_right);
          leanLeftView = (TextView) findViewById(R.id.lean_left);
          leanRightView = (TextView) findViewById(R.id.lean_right);
          reloadView = (TextView) findViewById(R.id.reload);
          pickUpObjectView = (TextView) findViewById(R.id.pick_up_object);
          celebrateView = (TextView) findViewById(R.id.celebrate);
          swapWeaponView = (TextView) findViewById(R.id.swap_weapon);
          shieldView = (TextView) findViewById(R.id.shield);
          knifeAttackView = (TextView) findViewById(R.id.knife_attack);

          multiplierXText = (EditText) findViewById(R.id.multiplier_x);
          multiplierYText = (EditText) findViewById(R.id.multiplier_y);
          multiplierZText = (EditText) findViewById(R.id.multiplier_z);


          strengthBar = (SeekBar) findViewById(R.id.strength_bar);
          strengthText = (TextView) findViewById(R.id.strength_value);

          strengthBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
          {
               @Override
               public void onProgressChanged(SeekBar seekBar, int i, boolean b)
               {
                    strengthText.setText(i + "");
               }

               @Override
               public void onStartTrackingTouch(SeekBar seekBar)
               {

               }

               @Override
               public void onStopTrackingTouch(SeekBar seekBar)
               {
                    ArkadeBlaster.setFilterStrength((byte) strengthBar.getProgress());
               }
          });

          strengthBar.setMax(100);
          strengthBar.setProgress(20);

     }
     public void onResetMotionStates(final View view)
     {
          ArkadeBlaster.resetMotionState() ;
     }

     public void onLedControl(final View view)
     {
          Intent myIntent = new Intent(this, GameControlActivity.class);
          this.startActivity(myIntent);
     }

     public void updateUI()
     {
          try
          {
               ArkadeBlaster.eulerMultiplier.x = Float.parseFloat(multiplierXText.getText().toString());
               ArkadeBlaster.eulerMultiplier.y = Float.parseFloat(multiplierYText.getText().toString());
               ArkadeBlaster.eulerMultiplier.z = Float.parseFloat(multiplierZText.getText().toString());
          }
          catch (Exception e)
          {}

          deviceStatusView.setText(ArkadeBlaster.isDeviceConnected()?"True":"False");
          eulerXView.setText(ArkadeBlaster.Inputs.eulerAngle.x + "°");
          eulerYView.setText(ArkadeBlaster.Inputs.eulerAngle.y + "°");
          eulerZView.setText(ArkadeBlaster.Inputs.eulerAngle.z + "°");
          linearXView.setText(ArkadeBlaster.Inputs.linearVelocity.x + "cm/s");
          linearYView.setText(ArkadeBlaster.Inputs.linearVelocity.y + "cm/s");
          linearZView.setText(ArkadeBlaster.Inputs.linearVelocity.z + "cm/s");
          angularXView.setText(ArkadeBlaster.Inputs.angularVelocity.x + "°/s");
          angularYView.setText(ArkadeBlaster.Inputs.angularVelocity.y + "°/s");
          angularZView.setText(ArkadeBlaster.Inputs.angularVelocity.z + "°/s");
          buttonAView.setText(ArkadeBlaster.Inputs.buttonA.key?"True":"False");
          buttonBView.setText(ArkadeBlaster.Inputs.buttonB.key?"True":"False");
          buttonCView.setText(ArkadeBlaster.Inputs.buttonC.key?"True":"False");
          buttonDView.setText(ArkadeBlaster.Inputs.buttonD.key?"True":"False");
          buttonEView.setText(ArkadeBlaster.Inputs.buttonE.key?"True":"False");
          buttonFView.setText(ArkadeBlaster.Inputs.buttonF.key?"True":"False");
          buttonXView.setText(ArkadeBlaster.Inputs.buttonX.key?"True":"False");
          buttonYView.setText(ArkadeBlaster.Inputs.buttonY.key?"True":"False");
          thumb1View.setText(ArkadeBlaster.Inputs.thumb1.key?"True":"False");
          thumb2View.setText(ArkadeBlaster.Inputs.thumb2.key?"True":"False");
          fireView.setText(ArkadeBlaster.Inputs.fire.key?"True":"False");
          backwardView.setText(ArkadeBlaster.Inputs.backward.key?"True":"False");
          forward1View.setText(ArkadeBlaster.Inputs.forward1.key?"True":"False");
          forward2View.setText(ArkadeBlaster.Inputs.forward2.key?"True":"False");

          moveUpView.setText(ArkadeBlaster.Inputs.moveUp?"True":"False");
          moveDownView.setText(ArkadeBlaster.Inputs.moveDown?"True":"False");
          moveLeftView.setText(ArkadeBlaster.Inputs.moveLeft?"True":"False");
          moveRightView.setText(ArkadeBlaster.Inputs.moveRight?"True":"False");
          moveForwardView.setText(ArkadeBlaster.Inputs.moveForward?"True":"False");
          moveBackwardView.setText(ArkadeBlaster.Inputs.moveBackward?"True":"False");
          tap.setText(ArkadeBlaster.Inputs.tap?"True":"False");
          turnLeftView.setText(ArkadeBlaster.Inputs.turnLeft?"True":"False");
          turnRightView.setText(ArkadeBlaster.Inputs.turnRight?"True":"False");
          leanLeftView.setText(ArkadeBlaster.Inputs.leanLeft?"True":"False");;
          leanRightView.setText(ArkadeBlaster.Inputs.leanRight?"True":"False");;
          reloadView.setText(ArkadeBlaster.Inputs.reload?"True":"False");;
          pickUpObjectView.setText(ArkadeBlaster.Inputs.pickUpObject?"True":"False");;
          celebrateView.setText(ArkadeBlaster.Inputs.celebrate?"True":"False");;
          swapWeaponView.setText(ArkadeBlaster.Inputs.swapWeapon?"True":"False");;
          knifeAttackView.setText(ArkadeBlaster.Inputs.knifeAttack?"True":"False");;
          shieldView.setText(ArkadeBlaster.Inputs.shield?"True":"False");;
     }

     Runnable uiUpdate = new Runnable()
     {
          public void run()
          {

               runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         updateUI();
                    }
               });

               uiUpdateHandler.postDelayed(this, 30);

          }
     };

}
