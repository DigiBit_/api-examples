// ========================================================================================
// 2018 Copyright © DigiBit LLC - All rights reserved
//
package games.arkade.libraryexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import games.arkade.arkadeblaster.ArkadeBlaster;

public class GameControlActivity extends AppCompatActivity
{

     SeekBar healthBar;
     TextView healthText;

     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_game_control);
          Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
          setSupportActionBar(toolbar);
          healthBar = (SeekBar) findViewById(R.id.health_bar);
          healthText = (TextView) findViewById(R.id.health_value);

          healthBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
          {
               @Override
               public void onProgressChanged(SeekBar seekBar, int i, boolean b)
               {
                    healthText.setText(i + "");
               }

               @Override
               public void onStartTrackingTouch(SeekBar seekBar)
               {

               }

               @Override
               public void onStopTrackingTouch(SeekBar seekBar)
               {

               }
          });

          healthBar.setMax(100);
          healthBar.setProgress(100);
     }

     public void onStartEndGame(final View view)
     {
          if (view.getId() == R.id.victory)
               ArkadeBlaster.startEndGame(ArkadeBlaster.GameStates.VICTORY) ;
          else if (view.getId() == R.id.defeat)
               ArkadeBlaster.startEndGame(ArkadeBlaster.GameStates.DEFEAT) ;
          else if (view.getId() == R.id.start)
               ArkadeBlaster.startEndGame(ArkadeBlaster.GameStates.START) ;
     }

     public void onSetHealth(final View view)
     {
          int health = healthBar.getProgress();
          if (view.getId() == R.id.none)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.NONE) ;
          else if (view.getId() == R.id.healing)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.HEALING) ;
          else if (view.getId() == R.id.poison_attack)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.POISON_ATTACK) ;
          else if (view.getId() == R.id.taking_damage)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.TAKING_DAMAGE) ;
          else if (view.getId() == R.id.explosive)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.EXPLOSIVE) ;
          else if (view.getId() == R.id.flash_bangs)
               ArkadeBlaster.setHealth(health, ArkadeBlaster.DamageStates.FLASH_BANGS) ;

     }
}
