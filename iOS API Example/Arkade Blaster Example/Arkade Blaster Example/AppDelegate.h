// ========================================================================================
// AppDelegate.h
// Arkade Blaster Example
// 2018 Copyright © DigiBit LLC - All rights reserved
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

