// ========================================================================================
// ArkadeBlaster.h
// Arkade Blaster Library
// 2018 Copyright © DigiBit LLC - All rights reserved
//

#import <Foundation/Foundation.h>

typedef enum {
     GAME_STATE_VICTORY   = 0x01,
     GAME_STATE_DEFEAT, //= 0x02
     GAME_STATE_START,  //= 0x03
} GameStates;

typedef enum {
     DAMAGE_STATE_NONE             = 0x01,
     DAMAGE_STATE_HEALING,       //= 0x02
     DAMAGE_STATE_POISON_ATTACK, //= 0x03
     DAMAGE_STATE_TAKING_DAMAGE, //= 0x04
     DAMAGE_STATE_EXPLOSIVE,     //= 0x05
     DAMAGE_STATE_FLASH_BANGS,   //= 0x06
} DamageStates;

typedef enum {
     A = 0x01,
     B, C, D, E, F, X, Y, UP, DOWN, TRIGGER, BACKWARD, FORWARD1, FORWARD2, NONE
     
} Buttons;

struct Button
{
     bool key;
     bool keyDown;
     bool keyUp;
};

struct Vector3
{
     float x;
     float y;
     float z;
};
struct ArkadeBlasterInputs
{
     int timeStamp;
     struct Button buttonX;
     struct Button buttonY;
     struct Button buttonA;
     struct Button buttonB;
     struct Button buttonC;
     struct Button buttonD;
     struct Button buttonE;
     struct Button buttonF;
     struct Button fire;
     struct Button thumb1;
     struct Button thumb2;
     struct Button forward1;
     struct Button forward2;
     struct Button backward;

     struct Vector3 linearVelocity;
     struct Vector3 eulerAngle;
     struct Vector3 angularVelocity;

     bool turnLeft;
     bool turnRight;
     bool moveUp;
     bool moveDown;
     bool moveLeft;
     bool moveRight;
     bool moveForward;
     bool moveBackward;
     bool tap;
     bool leanLeft;
     bool leanRight;
     bool reload;
     bool pickUpObject;
     bool celebrate;
     bool swapWeapon;
     bool shield;
     bool knifeAttack;
};

@protocol ArkadeBlasterDelegate <NSObject>
@optional
- (void) onConnected;
- (void) onDisconnected;
- (void) onResetMotionResponse:(bool) reset;
@end

@interface ArkadeBlaster:NSObject
+(void) init: (bool) logsEnabled delegate:(id)delegate;
+(struct ArkadeBlasterInputs) inputs;
+(void) setEulerMultiplier: (struct Vector3) multiplier;
+(bool) isDeviceConnected;
+(bool) resetMotionState;
+(bool) setFilterStrength: (uint8_t) strength;
+(bool) startEndGame: (GameStates) gameState;
+(bool) setAimDownSightsButton: (Buttons) button;
+(bool) setHealth: (uint8_t) health damageState: (DamageStates) damageState;
+(bool) setHighResMode: (bool) hires;
@end
