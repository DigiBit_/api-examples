// ========================================================================================
// LedControlViewController.m
// Arkade Blaster Library
// 2018 Copyright © DigiBit LLC - All rights reserved
//
#import "LedControlViewController.h"
#import "ArkadeBlaster.h"

@interface LedControlViewController ()
@property (weak, nonatomic) IBOutlet UILabel *playerHealth;
@property (weak, nonatomic) IBOutlet UISlider *healthController;
@end

@implementation LedControlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)onVictoryCLicked:(id)sender {
     [ArkadeBlaster startEndGame:GAME_STATE_VICTORY];
}

- (IBAction)onDefeatClicked:(id)sender {
     [ArkadeBlaster startEndGame:GAME_STATE_DEFEAT ];
}

- (IBAction)onStartClicked:(id)sender {
     [ArkadeBlaster startEndGame:GAME_STATE_START ];
}

- (IBAction)onNoneClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_NONE];
}

- (IBAction)onHealingClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_HEALING];
}

- (IBAction)onPoisonAttackClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_POISON_ATTACK];
}

- (IBAction)onTakingDamageClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_TAKING_DAMAGE];
}

- (IBAction)onExplosiveClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_EXPLOSIVE];
}

- (IBAction)onFlashBangsClicked:(id)sender {
     [ArkadeBlaster setHealth:floor(self.healthController.value) damageState:DAMAGE_STATE_FLASH_BANGS];
}

- (IBAction)onSliderValueChanged:(id)sender {
     self.playerHealth.text=[NSString stringWithFormat:@"%u", (uint)floor(self.healthController.value)];
}


@end
