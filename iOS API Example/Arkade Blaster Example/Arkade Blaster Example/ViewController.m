// ========================================================================================
// ViewController.m
// Arkade Blaster Example
// 2018 Copyright © DigiBit LLC - All rights reserved
//

#import "ViewController.h"

@interface ViewController () <ArkadeBlasterDelegate>
@property (weak, nonatomic) IBOutlet UILabel *deviceConnected;
@property (weak, nonatomic) IBOutlet UILabel *eulerX;
@property (weak, nonatomic) IBOutlet UILabel *eulerY;
@property (weak, nonatomic) IBOutlet UILabel *eulerZ;
@property (weak, nonatomic) IBOutlet UILabel *buttonA;
@property (weak, nonatomic) IBOutlet UILabel *buttonB;
@property (weak, nonatomic) IBOutlet UILabel *buttonC;
@property (weak, nonatomic) IBOutlet UILabel *buttonD;
@property (weak, nonatomic) IBOutlet UILabel *buttonE;
@property (weak, nonatomic) IBOutlet UILabel *buttonF;
@property (weak, nonatomic) IBOutlet UILabel *buttonX;
@property (weak, nonatomic) IBOutlet UILabel *buttonY;
@property (weak, nonatomic) IBOutlet UILabel *thumb1;
@property (weak, nonatomic) IBOutlet UILabel *thumb2;
@property (weak, nonatomic) IBOutlet UILabel *fire;
@property (weak, nonatomic) IBOutlet UILabel *backward;
@property (weak, nonatomic) IBOutlet UILabel *forward1;
@property (weak, nonatomic) IBOutlet UILabel *forward2;
@property (weak, nonatomic) IBOutlet UILabel *velocityX;
@property (weak, nonatomic) IBOutlet UILabel *velcoityY;
@property (weak, nonatomic) IBOutlet UILabel *velocityZ;
@property (weak, nonatomic) IBOutlet UILabel *moveUp;
@property (weak, nonatomic) IBOutlet UILabel *moveDown;
@property (weak, nonatomic) IBOutlet UILabel *moveLeft;
@property (weak, nonatomic) IBOutlet UILabel *moveRight;
@property (weak, nonatomic) IBOutlet UILabel *moveForward;
@property (weak, nonatomic) IBOutlet UILabel *moveBackward;
@property (weak, nonatomic) IBOutlet UILabel *tap;
@property (weak, nonatomic) IBOutlet UILabel *turnRight;
@property (weak, nonatomic) IBOutlet UILabel *turnLeft;
@property (weak, nonatomic) IBOutlet UILabel *leanLeft;
@property (weak, nonatomic) IBOutlet UILabel *leanRight;
@property (weak, nonatomic) IBOutlet UILabel *reload;
@property (weak, nonatomic) IBOutlet UILabel *pickUpObject;
@property (weak, nonatomic) IBOutlet UILabel *celebrate;
@property (weak, nonatomic) IBOutlet UILabel *swapWeapon;
@property (weak, nonatomic) IBOutlet UILabel *shield;
@property (weak, nonatomic) IBOutlet UILabel *knifeAttack;
@property (weak, nonatomic) IBOutlet UILabel *angularX;
@property (weak, nonatomic) IBOutlet UILabel *angularY;
@property (weak, nonatomic) IBOutlet UILabel *angularZ;
@property (weak, nonatomic) IBOutlet UITextField *multiplierX;
@property (weak, nonatomic) IBOutlet UITextField *multiplierY;
@property (weak, nonatomic) IBOutlet UITextField *multiplierZ;
@property (weak, nonatomic) IBOutlet UILabel *strengthValue;
@property (weak, nonatomic) IBOutlet UISlider *strengthSlider;

@end

@implementation ViewController

- (void)viewDidLoad
{
     [super viewDidLoad];
     [ArkadeBlaster init:false delegate: self];
     [NSTimer scheduledTimerWithTimeInterval:0.016 target:self selector:@selector(updateData) userInfo:nil repeats:YES];
}

-(void)updateData
{
     self.deviceConnected.text = [ArkadeBlaster isDeviceConnected]?@"True":@"False";
     self.eulerX.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].eulerAngle.x];
     self.eulerY.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].eulerAngle.y];
     self.eulerZ.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].eulerAngle.z];
     self.angularX.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].angularVelocity.x];
     self.angularY.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].angularVelocity.y];
     self.angularZ.text = [NSString stringWithFormat:@"%1.2f°", [ArkadeBlaster inputs].angularVelocity.z];
     self.buttonA.text = [ArkadeBlaster inputs].buttonA.key?@"True":@"False";
     self.buttonB.text = [ArkadeBlaster inputs].buttonB.key?@"True":@"False";
     self.buttonC.text = [ArkadeBlaster inputs].buttonC.key?@"True":@"False";
     self.buttonD.text = [ArkadeBlaster inputs].buttonD.key?@"True":@"False";
     self.buttonE.text = [ArkadeBlaster inputs].buttonE.key?@"True":@"False";
     self.buttonF.text = [ArkadeBlaster inputs].buttonF.key?@"True":@"False";
     self.buttonX.text = [ArkadeBlaster inputs].buttonX.key?@"True":@"False";
     self.buttonY.text = [ArkadeBlaster inputs].buttonY.key?@"True":@"False";
     self.thumb1.text = [ArkadeBlaster inputs].thumb1.key?@"True":@"False";
     self.thumb2.text = [ArkadeBlaster inputs].thumb2.key?@"True":@"False";
     self.fire.text = [ArkadeBlaster inputs].fire.key?@"True":@"False";
     self.backward.text = [ArkadeBlaster inputs].backward.key?@"True":@"False";
     self.forward1.text = [ArkadeBlaster inputs].forward1.key?@"True":@"False";
     self.forward2.text = [ArkadeBlaster inputs].forward2.key?@"True":@"False";

     self.velocityX.text = [NSString stringWithFormat:@"%1.2fcm/s", [ArkadeBlaster inputs].linearVelocity.x];
     self.velcoityY.text = [NSString stringWithFormat:@"%1.2fcm/s", [ArkadeBlaster inputs].linearVelocity.y];
     self.velocityZ.text = [NSString stringWithFormat:@"%1.2fcm/s", [ArkadeBlaster inputs].linearVelocity.z];
     self.moveUp.text = [ArkadeBlaster inputs].moveUp?@"True":@"False";
     self.moveDown.text = [ArkadeBlaster inputs].moveDown?@"True":@"False";
     self.moveLeft.text = [ArkadeBlaster inputs].moveLeft?@"True":@"False";
     self.moveRight.text = [ArkadeBlaster inputs].moveRight?@"True":@"False";
     self.moveForward.text = [ArkadeBlaster inputs].moveForward?@"True":@"False";
     self.moveBackward.text = [ArkadeBlaster inputs].moveBackward?@"True":@"False";
     self.tap.text = [ArkadeBlaster inputs].tap?@"True":@"False";
     self.turnRight.text = [ArkadeBlaster inputs].turnRight?@"True":@"False";
     self.turnLeft.text = [ArkadeBlaster inputs].turnLeft?@"True":@"False";
     self.leanLeft.text = [ArkadeBlaster inputs].leanLeft?@"True":@"False";
     self.leanRight.text = [ArkadeBlaster inputs].leanRight?@"True":@"False";
     self.reload.text = [ArkadeBlaster inputs].reload?@"True":@"False";
     self.pickUpObject.text = [ArkadeBlaster inputs].pickUpObject?@"True":@"False";
     self.celebrate.text = [ArkadeBlaster inputs].celebrate?@"True":@"False";
     self.swapWeapon.text = [ArkadeBlaster inputs].swapWeapon?@"True":@"False";
     self.shield.text = [ArkadeBlaster inputs].shield?@"True":@"False";
     self.knifeAttack.text = [ArkadeBlaster inputs].knifeAttack?@"True":@"False";
}

- (void)didReceiveMemoryWarning
{
     [super didReceiveMemoryWarning];

}
- (IBAction)onResetMotionStateClicked:(id)sender
{
     [ArkadeBlaster startEndGame:GAME_STATE_VICTORY];
     [ArkadeBlaster resetMotionState];
}

///////////////////////////////////////////////////////////////////////////////////////
// onConnected
// ====================================================================================
// Called when this app connects to the Arkade hardware over BLE.
//
-(void) onConnected
{
     NSLog(@"Arkade Blaster Connected");
}

////////////////////////////////////////////////////////////////////////////////////////
// onDisconnected
// =====================================================================================
// Called whn this app connects to the Arkade hardware over BLE.
//
-(void) onDisconnected
{
     NSLog(@"Arkade Blaster Disconnected");
}

////////////////////////////////////////////////////////////////////////////////////////
// onResetMotionResponse
// =====================================================================================
// called when a ResetMotion command is sent to teh Arkade hardware.
// This is used to verify that the command is correctly executed.
// reset = true; means the hardware recieved and executed the command.
// reset = false; means the hardware did not correctly execute the command.
//
-(void) onResetMotionResponse:(bool)reset
{
     NSLog(@"Reset Motion State Response: %d", reset );
}
- (IBAction)multiplierXUpdated:(id)sender
{
     [_multiplierX resignFirstResponder];
     [self updateMultiplier];
}
- (IBAction)multiplierYUpdated:(id)sender
{
     [_multiplierY resignFirstResponder];
     [self updateMultiplier];
}
- (IBAction)multiplierZUpdated:(id)sender
{
     [_multiplierZ resignFirstResponder];
     [self updateMultiplier];
}
- (IBAction)sliderValueChanged:(id)sender {
     self.strengthValue.text=[NSString stringWithFormat:@"%u", (uint)floor(self.strengthSlider.value)];
     [ArkadeBlaster setFilterStrength:floor(self.strengthSlider.value)];
}

-(void) updateMultiplier
{
     @try
     {
          struct Vector3 multiplier;
          NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
          numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;

          multiplier.x = [numberFormatter numberFromString:[_multiplierX text]].floatValue;
          multiplier.y = [numberFormatter numberFromString:[_multiplierY text]].floatValue;
          multiplier.z = [numberFormatter numberFromString:[_multiplierZ text]].floatValue;

          [ArkadeBlaster setEulerMultiplier:multiplier];
     } @catch (NSException *exception) {

     } @finally {

     }

}

@end
